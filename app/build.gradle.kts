import com.github.ramonrabello.marvelappcompose.AndroidSDK
import com.github.ramonrabello.marvelappcompose.Deps
import com.github.ramonrabello.marvelappcompose.Versions

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    kotlin("plugin.serialization") version "1.9.22"
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
    //id("com.google.devtools.ksp")
}

android {
    namespace = "com.github.ramonrabello.marvelappcompose"
    compileSdk = AndroidSDK.compileSdk

    defaultConfig {
        applicationId = "com.github.ramonrabello.marvelappcompose"
        minSdk = AndroidSDK.minSdk
        targetSdk = AndroidSDK.targetSdk
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        buildConfigField("String", "MARVEL_API_ENDPOINT_URL", "\"https://gateway.marvel.com/v1/public/\"")

        // build config fields that need to be set before running the app. For more details
        // on how to get your public and private keys, please go to
        // developer.marvel.com
        buildConfigField("String", "PUBLIC_KEY", "\"PUT_YOUR_PUBLIC_KEY_HERE\"")
        buildConfigField("String", "PRIVATE_KEY", "\"PUT_YOUR_PRIVATE_KEY_HERE\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    val coreDeps = Deps.core
    val networkDeps = Deps.network
    val composeDeps = Deps.compose
    val archDeps = Deps.arch
    val diDeps = Deps.di
    val testingDeps = Deps.testing

    // Core
    for (dependency in coreDeps) {
        implementation(dependency)
    }

    // Network
    for (dependency in networkDeps) {
        implementation(dependency)
    }

    // Compose
    implementation(platform("androidx.compose:compose-bom:2024.01.00"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")
    for (dependency in composeDeps) {
        implementation(dependency)
    }

    // Architecture Components
    for (dependency in archDeps) {
        implementation(dependency)
    }
    kapt("androidx.room:room-compiler:2.6.1")

    // Testing
    for (dependency in testingDeps) {
        testImplementation(dependency)
    }

    // DI
    for (dependency in diDeps) {
        implementation(dependency)
    }
    kapt("com.google.dagger:hilt-android-compiler:2.50")
    kapt("androidx.hilt:hilt-compiler:1.1.0")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2024.01.00"))

    // For instrumentation tests
    androidTestImplementation("com.google.dagger:hilt-android-testing:2.50")
    androidTestAnnotationProcessor("com.google.dagger:hilt-compiler:2.50")

    // For local unit tests
    testImplementation("com.google.dagger:hilt-android-testing:2.50")
    testAnnotationProcessor("com.google.dagger:hilt-compiler:2.50")
}

// Allow references to generated code
kapt {
    correctErrorTypes = true
}

configurations {
    "implementation" {
        resolutionStrategy.failOnVersionConflict()
    }
}

configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}