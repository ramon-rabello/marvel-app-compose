package com.github.ramonrabello.marvelappcompose.data.datasource

import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import kotlinx.coroutines.flow.Flow

/**
 * Interface to communicate with Marvel API Comics Endpoint
 */
interface ComicsDataSource {
    fun getComics(offset:Int = 0): Flow<List<ComicModel>>
    fun getComicById(id: Long): Flow<ComicModel>
}