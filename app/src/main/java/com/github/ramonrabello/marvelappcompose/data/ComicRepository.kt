package com.github.ramonrabello.marvelappcompose.data

import com.github.ramonrabello.marvelappcompose.data.datasource.ComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.data.di.Local
import com.github.ramonrabello.marvelappcompose.data.di.Remote
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class ComicRepository @Inject constructor(
    @Local private val local: ComicsDataSource,
    @Remote private val remote: ComicsDataSource,
    private val dispatcherProvider: DispatcherProvider
) {
    fun getComics(offset: Int = 0): Flow<List<ComicModel>> =
        remote.getComics(offset).flowOn(dispatcherProvider.io)

    fun getComicById(id: Long): Flow<ComicModel> =
        remote.getComicById(id).flowOn(dispatcherProvider.io)
}