package com.github.ramonrabello.marvelappcompose.data.datasource.remote

import com.github.ramonrabello.marvelappcompose.data.datasource.ComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.data.mapper.ComicDataWrapperToComicModelMapper
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class RemoteComicsDataSource @Inject constructor(
    private val api: MarvelApi,
    private val mapper: ComicDataWrapperToComicModelMapper,
    private val dispatcherProvider: DispatcherProvider
): ComicsDataSource {

    override fun getComics(offset: Int): Flow<List<ComicModel>> =
        flow {
            emit(mapper.mapToComicModels(api.getComics(offset)))
        }.flowOn(dispatcherProvider.io)

    override fun getComicById(id: Long): Flow<ComicModel> =
        flow {
            emit(mapper.mapToComicModel(api.getComicById(id)))
        }.flowOn(dispatcherProvider.io)

}