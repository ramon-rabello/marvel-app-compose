package com.github.ramonrabello.marvelappcompose.data.datasource.remote

import com.github.ramonrabello.marvelappcompose.BuildConfig

object RequestParams {
    const val TIMESTAMP = "ts"
    const val PUBLIC_KEY = "apikey"
    const val PRIVATE_KEY = BuildConfig.PRIVATE_KEY
    const val HASH = "hash"
    const val LIMIT = "limit"
}