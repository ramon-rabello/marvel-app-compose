package com.github.ramonrabello.marvelappcompose.data.datasource.remote

import com.github.ramonrabello.marvelappcompose.data.model.ComicDataWrapper
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MarvelApi {

    @GET("comics")
    suspend fun getComics(@Query("offset") offset: Int): ComicDataWrapper

    @GET("comics/{id}")
    suspend fun getComicById(@Path("id") id: Long): ComicDataWrapper
}