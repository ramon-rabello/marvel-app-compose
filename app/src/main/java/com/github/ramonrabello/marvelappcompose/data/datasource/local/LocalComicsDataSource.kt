package com.github.ramonrabello.marvelappcompose.data.datasource.local

import com.github.ramonrabello.marvelappcompose.data.datasource.ComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class LocalComicsDataSource @Inject constructor(
    private val comicsDao: ComicsDao,
    private val dispatcherProvider: DispatcherProvider
): ComicsDataSource {

    override fun getComics(offset: Int): Flow<List<ComicModel>> =
        flow { emit(comicsDao.getAllComics()) }
            .flowOn(dispatcherProvider.io)

    override fun getComicById(id: Long): Flow<ComicModel> =
        flow { emit(comicsDao.getComicById(id)) }
            .flowOn(dispatcherProvider.io)
}