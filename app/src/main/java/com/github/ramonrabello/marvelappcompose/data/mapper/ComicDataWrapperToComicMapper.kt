package com.github.ramonrabello.marvelappcompose.data.mapper

import com.github.ramonrabello.marvelappcompose.data.model.ComicData
import com.github.ramonrabello.marvelappcompose.data.model.ComicDataWrapper
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import java.util.Calendar
import javax.inject.Inject

class ComicDataWrapperToComicModelMapper @Inject constructor(){
    fun mapToComicModels(wrapper: ComicDataWrapper): List<ComicModel> {
        val comics = wrapper.data.results.map { comicData ->
            createComicModel(comicData)
        }
        return comics
    }

    fun mapToComicModel(wrapper: ComicDataWrapper): ComicModel =
            createComicModel(wrapper.data.results.first())

    private fun createComicModel(comicData: ComicData) : ComicModel {
        val dates = comicData.dates
        val prices = comicData.prices
        val creatorItems = comicData.creators.items

        return ComicModel(
            comicData.id,
            comicData.thumbnail.toString(),
            if (dates.isNotEmpty()) dates.first().date else Calendar.getInstance().time,
            comicData.title,
            comicData.description ?: "No description found for this comic.",
            if (creatorItems.isNotEmpty()) creatorItems.first().name else "No creator found for this comic.",
            if (prices.isNotEmpty()) prices.first().price else 0.0f
        )
    }
}