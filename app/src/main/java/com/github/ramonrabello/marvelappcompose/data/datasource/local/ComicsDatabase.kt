package com.github.ramonrabello.marvelappcompose.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.github.ramonrabello.marvelappcompose.data.datasource.local.ComicsDatabase.Companion.DATABASE_VERSION
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel

@Database(entities = [ComicModel::class], version = DATABASE_VERSION)
@TypeConverters(DateConverter::class)
abstract class ComicsDatabase:RoomDatabase() {
    abstract fun comicsDao(): ComicsDao

    companion object {
        const val DATABASE_NAME = "comics_db"
        const val DATABASE_VERSION = 1
    }
}