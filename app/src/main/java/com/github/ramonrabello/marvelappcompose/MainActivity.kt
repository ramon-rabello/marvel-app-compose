package com.github.ramonrabello.marvelappcompose

import android.os.Bundle
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.github.ramonrabello.marvelappcompose.ui.theme.components.ComicList
import com.github.ramonrabello.marvelappcompose.ui.HomeViewModel
import com.github.ramonrabello.marvelappcompose.ui.detail.ComicDetailViewModel
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.GetComics
import com.github.ramonrabello.marvelappcompose.ui.theme.MarvelAppComposeTheme
import com.github.ramonrabello.marvelappcompose.ui.theme.components.MarvelComposeApp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel:HomeViewModel by viewModels()
    private val comicDetailViewModel: ComicDetailViewModel by viewModels()
    private lateinit var navController:NavHostController

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        setContent {
            MarvelAppComposeTheme {
                navController = rememberNavController()
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    MarvelComposeApp(viewModel, comicDetailViewModel, navController){
                        navController.popBackStack()
                    }
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.onEvent(GetComics)
            }
        }
    }
}