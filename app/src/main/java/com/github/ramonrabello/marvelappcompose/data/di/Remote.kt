package com.github.ramonrabello.marvelappcompose.data.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Remote
