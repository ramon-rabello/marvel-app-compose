package com.github.ramonrabello.marvelappcompose.data.di

import android.content.Context
import com.github.ramonrabello.marvelappcompose.BuildConfig
import com.github.ramonrabello.marvelappcompose.data.ComicRepository
import com.github.ramonrabello.marvelappcompose.data.datasource.ComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.datasource.local.LocalComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.datasource.remote.JsonDateDeserializer
import com.github.ramonrabello.marvelappcompose.data.datasource.remote.MarvelApi
import com.github.ramonrabello.marvelappcompose.data.datasource.remote.RemoteComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.datasource.remote.RequestParamsInterceptor
import com.github.ramonrabello.marvelappcompose.data.mapper.ComicDataWrapperToComicModelMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.Date
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkingModule {

    @Provides
    @Singleton
    internal fun provideCache(@ApplicationContext context: Context): Cache {
        val cacheSize = 10 * 1024 * 1024
        val httpCacheDirectory = File(context.cacheDir, "http-cache")
        return Cache(httpCacheDirectory, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideCacheInterceptor(): Interceptor {
        return Interceptor { chain ->
            val response = chain.proceed(chain.request())
            val cacheControl = CacheControl.Builder().maxAge(1, TimeUnit.MINUTES).build()
            response.newBuilder()
                .header("Cache-Control", cacheControl.toString())
                .build()
        }
    }

    @Provides
    @Singleton
    fun provideRequestParamsInterceptor() = RequestParamsInterceptor()

    @Provides
    @Singleton
    fun provideHttpClient(
        cache: Cache,
        networkCacheInterceptor: Interceptor,
        requestParamsInterceptor: RequestParamsInterceptor): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor(networkCacheInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(requestParamsInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.MARVEL_API_ENDPOINT_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().registerTypeAdapter(Date::class.java, JsonDateDeserializer()).create()))
            .client(httpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideMarvelApi(retrofit: Retrofit): MarvelApi =
        retrofit.create(MarvelApi::class.java)

    @Provides
    @Singleton
    fun provideComicRepository(
        local: LocalComicsDataSource,
        remote: RemoteComicsDataSource,
        dispatcherProvider: DispatcherProvider): ComicRepository {
        return ComicRepository(local, remote, dispatcherProvider)
    }
}