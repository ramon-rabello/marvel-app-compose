package com.github.ramonrabello.marvelappcompose.ui

sealed class Screen(val route: String) {
    data object Home : Screen("home")
    data object ComicDetail : Screen("comic")
    data object Search: Screen("search")
}