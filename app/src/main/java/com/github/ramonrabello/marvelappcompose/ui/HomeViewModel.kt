package com.github.ramonrabello.marvelappcompose.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.ramonrabello.marvelappcompose.data.ComicRepository
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.CardClicked
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.GetComics
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.HomeEvent
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.HomeUiState
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.ComicsLoaded
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Empty
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.LoadMore
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Loading
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.NetworkError
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: ComicRepository
): ViewModel() {

    private val _comicsUiState = MutableStateFlow<HomeUiState>(Loading)
    val comicsUiState = _comicsUiState.asStateFlow()

    val allComics = mutableSetOf<ComicModel>()
    private var pageOffset = 0

    fun onEvent(event: HomeEvent) {
        when(event) {
            is GetComics -> handleGetComicsEvent()
            is CardClicked -> handleCardClickedEvent(event.comicId)
            is LoadMore -> handleGetComicsEvent()
            else -> {
                // invalid event passed
            }
        }
    }

    private fun handleCardClickedEvent(comicId: Long) {

    }

    private fun handleGetComicsEvent() {
        viewModelScope.launch {
            repository.getComics(pageOffset)
                .catch { error ->
                    _comicsUiState.value = NetworkError(error)
                }
                .collect { comics ->
                    if (!allComics.containsAll(comics)) {
                        allComics.addAll(comics)
                        pageOffset = allComics.size + 1
                        _comicsUiState.value =
                            if (comics.isEmpty()) Empty else ComicsLoaded(comics)
                    }
                    _comicsUiState.value =
                        if (comics.isEmpty()) Empty else ComicsLoaded(comics)
                }
        }
    }
}