package com.github.ramonrabello.marvelappcompose.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.ramonrabello.marvelappcompose.data.ComicRepository
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailEvent
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailLoaded
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailNetworkError
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailUiState
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.GetComicDetail
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.CardClicked
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.ComicsLoaded
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Empty
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailLoading
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.NavigateBack
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.NetworkError
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ComicDetailViewModel @Inject constructor(
    private val repository: ComicRepository
): ViewModel() {

    private val _comicDetailUiState = MutableStateFlow<ComicDetailUiState>(ComicDetailLoading)
    val comicDetailUiState = _comicDetailUiState.asStateFlow()

    fun onEvent(event: ComicDetailEvent) {
        when(event) {
            is GetComicDetail -> handleGetComicDetailEvent(event.comicId)
            is NavigateBack -> handleNavigateBackEvent()
        }
    }

    private fun handleNavigateBackEvent() {
        //_comicDetailUiState.value = NavigateBack
    }

    private fun handleGetComicDetailEvent(comicId: Long) {
        viewModelScope.launch {
            repository.getComicById(comicId)
                .catch { error ->
                    _comicDetailUiState.value = ComicDetailNetworkError(error)
                }
                .collect { comicModel ->
                    _comicDetailUiState.value = ComicDetailLoaded(comicModel)
                }
        }
    }
}