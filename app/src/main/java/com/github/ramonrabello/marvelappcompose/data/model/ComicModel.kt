package com.github.ramonrabello.marvelappcompose.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "comics_table")
data class ComicModel(
    @PrimaryKey(true) val id: Long,
    val image: String?,
    val publishingDate: Date?,
    val title: String,
    val description: String,
    val author: String,
    val price: Float
)