package com.github.ramonrabello.marvelappcompose

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MarvelAppComposeApplication: Application() {

}