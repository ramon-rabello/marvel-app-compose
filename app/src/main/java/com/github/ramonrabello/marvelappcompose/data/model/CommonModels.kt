package com.github.ramonrabello.marvelappcompose.data.model

import java.util.Date

/**
 * Models that are common and used by other models.
 */
data class ComicImage(val path: String, val extension: String) {
    override fun toString(): String = "$path.$extension"

    // landscape available modes
    val xlargeLand = "$path/xlarge_landscape.$extension"
    val largeLand = "$path/large_landscape.$extension"
    val mediumLand = "$path/medium_landscape.$extension"
    val smallLand = "$path/small_landscape.$extension"

    // portrait available modes
    val xlargePort = "$path/xlarge_portrait.$extension"
    val largePort = "$path/large_portrait.$extension"
    val mediumPort = "$path/medium_portrait.$extension"
    val smallPortrait = "$path/small_portrait.$extension"
}

data class ComicData(
    val id: Long,
    val title: String,
    val prices: List<ComicPrice>,
    val dates: List<ComicDate>,
    val creators: ComicCreator,
    val description: String?,
    val thumbnail: ComicImage
)

data class ComicPrice(
    val type: String,
    val price: Float
)

data class ComicDate(
    val type: String,
    val date: Date?
)

data class ComicCreator(
    val items: List<ComicCreatorItem>
)

data class ComicCreatorItem(
    val name: String,
    val role: String
)