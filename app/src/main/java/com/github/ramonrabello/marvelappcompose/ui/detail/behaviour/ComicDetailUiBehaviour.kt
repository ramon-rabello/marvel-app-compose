package com.github.ramonrabello.marvelappcompose.ui.detail.behaviour

import com.github.ramonrabello.marvelappcompose.data.model.ComicModel

/**
 * Represents the states regarding the Home screen.
 *
 * Loading = The app is loading data
 * ComicsLoaded = The comics should be loaded whether from local or remote data sources
 * Empty = No comics are available (ex: during search)
 * Error = An error has occurred
 */
sealed interface ComicDetailUiState
data object ComicDetailLoading: ComicDetailUiState
data class ComicDetailLoaded(val data: ComicModel): ComicDetailUiState
data object ComicDetailEmpty: ComicDetailUiState
data class ComicDetailNetworkError(private val error: Throwable): ComicDetailUiState

/**
 * Represents the events regarding the Home screen
 */
sealed interface ComicDetailEvent
data class GetComicDetail(val comicId: Long): ComicDetailEvent
data object NavigateBack: ComicDetailEvent