package com.github.ramonrabello.marvelappcompose.ui.home.behaviour

import com.github.ramonrabello.marvelappcompose.data.model.ComicModel

/**
 * Represents the states regarding the Home screen.
 *
 * Loading = The app is loading data
 * ComicsLoaded = The comics should be loaded whether from local or remote data sources
 * Empty = No comics are available (ex: during search)
 * Error = An error has occurred
 */
sealed interface HomeUiState
data object Loading: HomeUiState
data class ComicsLoaded(val data: List<ComicModel>): HomeUiState
data object Empty: HomeUiState
data class NetworkError(private val error: Throwable): HomeUiState

/**
 * Represents the events regarding the Home screen
 */
sealed interface HomeEvent
data object GetComics: HomeEvent
data object LoadMore: HomeEvent
data object SearchComics: HomeEvent
data class CardClicked(val comicId:Long): HomeEvent


