package com.github.ramonrabello.marvelappcompose.ui.common

import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.HomeUiState

sealed interface CommonUiState
data object Loading: CommonUiState
data class DataLoaded(val data: List<ComicModel>): CommonUiState
data object Empty: CommonUiState
data class NetworkError(private val error: Throwable): CommonUiState