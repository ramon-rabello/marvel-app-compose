package com.github.ramonrabello.marvelappcompose.ui.theme.components

import android.util.Log
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ComposableTarget
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.github.ramonrabello.marvelappcompose.R
import com.github.ramonrabello.marvelappcompose.data.datasource.remote.RequestParams
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import com.github.ramonrabello.marvelappcompose.ui.HomeViewModel
import com.github.ramonrabello.marvelappcompose.ui.Screen
import com.github.ramonrabello.marvelappcompose.ui.detail.ComicDetailViewModel
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailEmpty
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailLoaded
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailLoading
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailNetworkError
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.GetComicDetail
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.CardClicked
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Empty
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.ComicsLoaded
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.GetComics
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.LoadMore
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Loading
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.NetworkError

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MarvelComposeApp(
    homeViewModel: HomeViewModel = viewModel(),
    comicDetailViewModel: ComicDetailViewModel = viewModel(),
    navController: NavHostController,
    onBackButtonClick: () -> Unit
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
    Scaffold(
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Marvel Compose",
                        style = MaterialTheme.typography.headlineMedium
                    )
                },
                actions = {
                    IconButton(
                        onClick = { navController.navigate(Screen.Search.route) }
                    ) {
                        Icon(Icons.Default.Search, contentDescription = "Search")
                    }
                },
                scrollBehavior = scrollBehavior,
                navigationIcon = {
                    IconButton(onClick = { onBackButtonClick() }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = "Back button"
                        )
                    }
                },
                colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = Color.Red )
            )
        },
        content = { paddingValues ->
            NavGraph(navController, homeViewModel, comicDetailViewModel, paddingValues)
        }
    )
}

@Composable
fun NavGraph(
    navController: NavHostController,
    homeViewModel: HomeViewModel,
    comicDetailViewModel: ComicDetailViewModel,
    paddingValues: PaddingValues
) {
    NavHost(navController = navController, startDestination = Screen.Home.route) {
        composable(Screen.Home.route) {
            EnterAnimation {
                HomeScreen(homeViewModel, navController, paddingValues)
            }

        }
        composable(Screen.ComicDetail.route+"/{comicId}",
            arguments = listOf(
                navArgument("comicId") {
                    type = NavType.LongType
                }
            )) { navBackStackEntry ->
            val comicId = navBackStackEntry.arguments?.getLong("comicId")

            if (comicId == null) {
                Toast.makeText(
                    LocalContext.current,
                    "Comic id is required",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                comicDetailViewModel.onEvent(GetComicDetail(comicId))
                EnterAnimation {
                    ComicDetailScreen(comicDetailViewModel, paddingValues)
                }
            }
        }
        composable(Screen.Search.route) {
            Search(searchQuery = "", searchResults = emptyList(), paddingValues, onSearchQueryChange = {})
        }
    }
}

@Composable
fun HomeScreen(
    homeViewModel: HomeViewModel = viewModel(),
    navController: NavHostController,
    paddingValues: PaddingValues
) {
    Column(modifier = Modifier.padding(paddingValues)) {
        //Spacer(modifier = Modifier.height(8.dp))
        //Search(searchQuery = "", searchResults = emptyList(), onSearchQueryChange = {})
        //Spacer(modifier = Modifier.height(8.dp))
        ComicList(
            homeViewModel = homeViewModel,
            onLoading = { LoadingScreen() },
            onSuccess = { comics ->
                LazyVerticalGrid(columns = GridCells.Adaptive(minSize = 128.dp)) {
                    items(comics) { comic ->
                        ComicCard(
                            comic = comic,
                            onCardClick = { _ ->
                                navController.navigate("${Screen.ComicDetail.route}/${comic.id}")
                            }
                        )
                    }
                }
            },
            onError = { ErrorScreen(onTryAgainButtonClick = { homeViewModel.onEvent(GetComics) }) },
            onEmpty = { EmptyScreen() }
        )
        Spacer(modifier = Modifier.height(8.dp))
    }
}
    
@Composable
fun ComicList(
    homeViewModel: HomeViewModel,
    onLoading: @Composable () -> Unit,
    onSuccess: @Composable (List<ComicModel>) -> Unit,
    onEmpty: @Composable () -> Unit,
    onError: @Composable () -> Unit
) {
    val uiState by homeViewModel.comicsUiState.collectAsState(Loading)
    when(uiState) {
        is Loading -> onLoading()
        is ComicsLoaded -> onSuccess((uiState as ComicsLoaded).data)
        is NetworkError -> onError()
        is Empty -> onEmpty()
    }
}

@Composable
fun ComicCard(comic: ComicModel, onCardClick:(Long) -> Unit) {
    Card(
        modifier = Modifier
            .padding(2.dp)
            .width(100.dp)
            .height(150.dp)
            .clickable { onCardClick(comic.id) },
        shape = RoundedCornerShape(4.dp),
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            val showShimmer = remember { mutableStateOf(true) }
            AsyncImage(
                ImageRequest.Builder(LocalContext.current)
                    .data(comic.image)
                    .crossfade(true)
                    .build(),
                contentDescription = "Comic Cover",
                modifier = Modifier
                    .background(shimmerBrush(targetValue = 1300f, showShimmer = showShimmer.value))
                    .matchParentSize(),
                onSuccess = { showShimmer.value = false },
                onError = {
                          Log.e("AsyncImage", it.toString())
                },
                contentScale = ContentScale.Crop
            )
            Text(
                modifier = Modifier
                    .padding(16.dp)
                    .align(Alignment.BottomEnd),
                text = comic.title,
                style = MaterialTheme.typography.bodySmall,
                color = Color.White

            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Search(
    searchQuery: String,
    searchResults: List<ComicModel>,
    paddingValues: PaddingValues,
    onSearchQueryChange: (String) -> Unit
) {
    val search = remember { mutableStateOf("") }
    SearchBar(
        modifier = Modifier
            .padding(paddingValues)
            .wrapContentHeight(),
        query = searchQuery,
        onQueryChange = onSearchQueryChange,
        shape = RoundedCornerShape(4.dp),
        onSearch = { search.value = searchQuery },
        placeholder = {
            Text(text = "Search comics")
        },
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Search,
                tint = MaterialTheme.colorScheme.onSurface,
                contentDescription = "Search icon"
            )
        },
        trailingIcon = {
            Icon(
                imageVector = Icons.Default.Clear,
                tint = MaterialTheme.colorScheme.onSurface,
                contentDescription = "Clear icon"
            )
        },
        content = {
            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(32.dp),
                contentPadding = PaddingValues(16.dp),
                modifier = Modifier.wrapContentHeight()
            ) {
                items(
                    count = searchResults.size,
                    key = { index -> searchResults[index].id },
                    itemContent = { index ->
                        val comic = searchResults[index]
                        ComicListItem(comic = comic)
                    }
                )
            }
        },
        active = true,
        onActiveChange = {},
        tonalElevation = 2.dp
    )
}

@Composable
fun InfiniteScrollList(
    itemCount: Int = 20,
    onLoadMore: () -> Unit,
    content: @Composable (Int) -> Unit
) {
    val state = rememberLazyGridState()

    LazyVerticalGrid(state = state, columns = GridCells.Adaptive(minSize = 128.dp)) {
        items(itemCount) { index ->
            content(index)
            if (itemCount > 0 && index == itemCount - 1) {
                onLoadMore()
            }
        }
    }
}

@Composable
fun ComicListItem(
    comic: ComicModel,
    modifier: Modifier = Modifier
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = modifier.fillMaxWidth()
    ) {
        Row {
            ComicImage(comic)
            Column {
                Text(
                    text = comic.title,
                    style = MaterialTheme.typography.labelMedium
                )
                Text(
                    style = MaterialTheme.typography.labelSmall,
                    text = comic.price.toString()
                )
            }
        }
    }
}

@Composable
private fun ComicImage(comic: ComicModel) {
    val showShimmer = remember { mutableStateOf(true) }
    AsyncImage(
        ImageRequest.Builder(LocalContext.current)
            .data(comic.image)
            .crossfade(true)
            .build(),
        contentDescription = "Comic Cover",
        modifier = Modifier
            .background(shimmerBrush(targetValue = 1300f, showShimmer = showShimmer.value))
            .fillMaxSize(),
        onSuccess = { showShimmer.value = false },
        onError = {
            Log.e("AsyncImage", it.toString())
        },
        contentScale = ContentScale.Crop
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ComicDetailScreen(
    viewModel: ComicDetailViewModel,
    paddingValues: PaddingValues) {
    Box (modifier = Modifier.padding(paddingValues)) {
        val uiState by viewModel.comicDetailUiState.collectAsState()

        when (uiState) {
            is ComicDetailLoading -> LoadingScreen()
            is ComicDetailEmpty -> EmptyScreen()
            is ComicDetailLoaded -> {
                val comic = (uiState as ComicDetailLoaded).data
                val showShimmer = remember { mutableStateOf(true) }
                Column {
                    AsyncImage(
                        ImageRequest.Builder(LocalContext.current)
                            .data(comic.image)
                            .crossfade(true)
                            .build(),
                        contentDescription = "Comic Cover",
                        modifier = Modifier
                            .background(
                                shimmerBrush(
                                    targetValue = 1300f,
                                    showShimmer = showShimmer.value
                                )
                            )
                            .fillMaxWidth()
                            .height(300.dp),
                        onSuccess = { showShimmer.value = false },
                        onError = {
                            Log.e("AsyncImage", it.toString())
                        },
                        contentScale = ContentScale.Crop
                    )
                    Spacer(modifier = Modifier.height(16.dp))
                    Column(modifier = Modifier.padding(16.dp)) {
                        Text(
                            text = comic.title,
                            style = MaterialTheme.typography.headlineMedium
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = comic.description,
                            style = MaterialTheme.typography.headlineSmall
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Published on ${comic.publishingDate}",
                            style = MaterialTheme.typography.headlineSmall
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Author: ${comic.author}",
                            style = MaterialTheme.typography.headlineSmall
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Price: \$${comic.price}",
                            style = MaterialTheme.typography.headlineSmall
                        )
                    }
                }

            }

            is ComicDetailNetworkError -> ErrorScreen {
                //viewModel.onEvent(GetComicDetail(comicId))
            }
        }
    }
}

@Composable
fun EnterAnimation(content: @Composable () -> Unit) {
    AnimatedVisibility(
        visibleState = MutableTransitionState(
            initialState = false
        ).apply { targetState = true },
        modifier = Modifier,
        enter = slideInVertically(
            initialOffsetY = { -40 }
        ) + expandVertically(
            expandFrom = Alignment.Top
        ) + fadeIn(initialAlpha = 0.3f),
        exit = slideOutVertically() + shrinkVertically() + fadeOut(),
    ) {
        content()
    }
}

