package com.github.ramonrabello.marvelappcompose.data.di

import android.content.Context
import androidx.room.Room
import com.github.ramonrabello.marvelappcompose.data.datasource.local.ComicsDao
import com.github.ramonrabello.marvelappcompose.data.datasource.local.ComicsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideComicsDao(comicsDatabase: ComicsDatabase): ComicsDao {
        return comicsDatabase.comicsDao()
    }

    @Singleton
    @Provides
    fun provideComicsDatabase(@ApplicationContext appContext: Context): ComicsDatabase {
        return Room.databaseBuilder(
            appContext,
            ComicsDatabase::class.java,
            ComicsDatabase.DATABASE_NAME
        ).allowMainThreadQueries()
            .build()
    }

    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }
}