package com.github.ramonrabello.marvelappcompose.data.datasource.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel

@Dao
interface ComicsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comic: ComicModel)

    @Update
    fun update(comic: ComicModel)

    @Delete
    fun delete(comic: ComicModel)

    @Query("SELECT * FROM comics_table")
    fun getAllComics(): List<ComicModel>

    @Query("SELECT * FROM comics_table WHERE id = :id")
    fun getComicById(id: Long): ComicModel
}