package com.github.ramonrabello.marvelappcompose.ui.home

import com.github.ramonrabello.marvelappcompose.data.ComicRepository
import com.github.ramonrabello.marvelappcompose.ui.HomeViewModel
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.ComicsLoaded
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.Empty
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.GetComics
import com.github.ramonrabello.marvelappcompose.ui.home.behaviour.NetworkError
import com.github.ramonrabello.marvelappcompose.util.TestUtil
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.io.IOException

class HomeViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private val repository = mockk<ComicRepository>()
    private lateinit var viewModel: HomeViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun initTest() {
        viewModel = HomeViewModel(repository)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun finishTest() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when onEvent called with GetComics event should emit ComicsLoaded state`() {
        runTest {
            val expectedComicList = TestUtil.createComicModelList()
            val comicListFlow = flowOf(expectedComicList)
            coEvery { repository.getComics() } returns comicListFlow

            viewModel.onEvent(GetComics)

            coVerify { repository.getComics() }

            assertTrue(viewModel.comicsUiState.value is ComicsLoaded)
            val data = (viewModel.comicsUiState.value as ComicsLoaded).data

            assertSame(data, expectedComicList)
        }
    }

    @Test
    fun `when onEvent called with GetComics event should emit Empty state`() {
        runTest {
            val expectedEmptyList = TestUtil.createComicModelEmptyList()
            val comicListFlow = flowOf(expectedEmptyList)
            coEvery { repository.getComics() } returns comicListFlow

            viewModel.onEvent(GetComics)

            coVerify { repository.getComics() }

            assertEquals(viewModel.comicsUiState.value, Empty)
        }
    }

    @Test
    fun `when onEvent called with GetComics event should emit NetworkError state`() {
        runTest {
            coEvery { repository.getComics() } throws IOException()

            viewModel.onEvent(GetComics)

            coVerify { repository.getComics() }

            assertTrue(viewModel.comicsUiState.value is NetworkError)

        }
    }
}