package com.github.ramonrabello.marvelappcompose.data

import com.github.ramonrabello.marvelappcompose.data.datasource.ComicsDataSource
import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.util.TestUtil
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class ComicRepositoryTest {

    private val local = mockk<ComicsDataSource>()
    private val remote = mockk<ComicsDataSource>()
    private val dispatcherProvider = mockk<DispatcherProvider>()
    private lateinit var repository: ComicRepository

    @Before
    fun initTest() {
        repository = ComicRepository(local, remote, dispatcherProvider)
    }

    @Test
    fun `when getComics called should check if both local and remote data sources were called`() {
        runTest {
            val expectedComicList = TestUtil.createComicModelList()
            coEvery { local.getComics() } returns flowOf(expectedComicList)
            coEvery { remote.getComics() } returns flowOf(expectedComicList)
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            repository.getComics().collect { comics ->
                assertTrue(comics.size == expectedComicList.size)
            }

            //coVerify { local.getComics() }
            coVerify { remote.getComics() }
        }
    }

    @Test
    fun `when getComicDetails called should check if both local and remote data sources were called`() {
        runTest {
            val singleComicModel = TestUtil.createSingleComicModel(1)
            coEvery { local.getComicById(any()) } returns flowOf(singleComicModel)
            coEvery { remote.getComicById(any()) } returns flowOf(singleComicModel)
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            repository.getComicById(1).collect { details ->
                assertSame(details, singleComicModel)
            }

            coVerify { remote.getComicById(1) }
        }
    }
}