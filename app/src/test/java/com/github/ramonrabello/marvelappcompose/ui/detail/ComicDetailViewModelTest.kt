package com.github.ramonrabello.marvelappcompose.ui.detail

import com.github.ramonrabello.marvelappcompose.data.ComicRepository
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailLoaded
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.ComicDetailNetworkError
import com.github.ramonrabello.marvelappcompose.ui.detail.behaviour.GetComicDetail
import com.github.ramonrabello.marvelappcompose.util.TestUtil
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import java.io.IOException

class ComicDetailViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private val repository = mockk<ComicRepository>()
    private lateinit var viewModel: ComicDetailViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun initTest() {
        viewModel = ComicDetailViewModel(repository)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun finishTest() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when onEvent called with GetComicDetail event should emit ComicDetailLoaded state`() {
        runTest {
            val expectedSingleComicModel = TestUtil.createSingleComicModel(1)
            val comicModelFlow = flowOf(expectedSingleComicModel)
            val comicIdSlot = slot<Long>()
            coEvery { repository.getComicById(capture(comicIdSlot)) } returns comicModelFlow

            viewModel.onEvent(GetComicDetail(comicIdSlot.captured))

            coVerify { repository.getComicById(comicIdSlot.captured) }

            assertTrue(viewModel.comicDetailUiState.value is ComicDetailLoaded)
            val data = (viewModel.comicDetailUiState.value as ComicDetailLoaded).data

            assertSame(data, expectedSingleComicModel)
        }
    }

    @Test
    fun `when onEvent called with GetComics event should emit NetworkError state`() {
        runTest {
            coEvery { repository.getComicById(any()) } throws IOException()

            viewModel.onEvent(GetComicDetail(1))

            coVerify { repository.getComics() }

            assertTrue(viewModel.comicDetailUiState.value is ComicDetailNetworkError)

        }
    }
}