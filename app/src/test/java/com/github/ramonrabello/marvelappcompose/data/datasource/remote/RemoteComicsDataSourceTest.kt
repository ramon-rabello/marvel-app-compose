package com.github.ramonrabello.marvelappcompose.data.datasource.remote

import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.data.mapper.ComicDataWrapperToComicModelMapper
import com.github.ramonrabello.marvelappcompose.data.model.ComicDataWrapper
import com.github.ramonrabello.marvelappcompose.util.TestUtil
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.test.runTest
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.IOException

class RemoteComicsDataSourceTest {

    private val api = mockk<MarvelApi>()
    private val mapper = mockk<ComicDataWrapperToComicModelMapper>(relaxed = true)
    private val dispatcherProvider = mockk<DispatcherProvider>()
    private lateinit var remote: RemoteComicsDataSource

    @Before
    fun initTest() {
        remote = RemoteComicsDataSource(api, mapper, dispatcherProvider)
    }

    @Test
    fun `when getComics called should retrieve the comic list from api`() {
        runTest {
            val expectedComicWrapper = TestUtil.createComicDataWrapper(1)
            val expectedComicModelList = TestUtil.createComicModelList()

            coEvery { api.getComics(0) } returns expectedComicWrapper
            coEvery { mapper.mapToComicModels(expectedComicWrapper) } returns expectedComicModelList
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            remote.getComics().collect { comics ->
                assertThat(comics, equalTo(expectedComicModelList))
            }

            coVerify { api.getComics(0) }
            coVerify { mapper.mapToComicModels(expectedComicWrapper) }
        }
    }

    @Test
    fun `when getComics called should retrieve an empty list`() {
        runTest {
            val mockComicDataWrapper = mockk<ComicDataWrapper>(relaxed = true)
            coEvery { api.getComics(0) } returns mockComicDataWrapper
            coEvery { mockComicDataWrapper.data.results } returns emptyList()
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            remote.getComics().collect { comics ->
                assertTrue(comics.isEmpty())
            }

            coVerify { api.getComics(0) }
        }
    }

    @Test
    fun `when getComics called should throw an exception`() {
        runTest {
            val mockComicDataWrapper = TestUtil.createComicDataWrapper(1)
            val comicModels = TestUtil.createComicModelList()
            coEvery { api.getComics(0) } throws IOException()
            coEvery { mapper.mapToComicModels(mockComicDataWrapper) } returns comicModels
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            remote.getComics().catch { exception ->
                assertTrue(exception is IOException)
            }.collect { _ ->

            }

            coVerify { api.getComics(0) }
            coVerify(exactly = 0) { mapper.mapToComicModels(mockComicDataWrapper) }
        }
    }

    @Test
    fun `when getComicById called should retrieve a single comic`() {
        runTest {
            val comicIdSlot = slot<Long>()
            val expectedComicDataWrapper = TestUtil.createComicDataWrapper(4)
            val singleComicModel = TestUtil.createSingleComicModel(4)
            coEvery { api.getComicById(capture(comicIdSlot)) } returns expectedComicDataWrapper
            coEvery { mapper.mapToComicModel(expectedComicDataWrapper) } returns singleComicModel
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            remote.getComicById(4).collect { comicModel ->
                assertThat(comicIdSlot.captured, equalTo(comicModel.id))
                assertThat(comicModel, equalTo(singleComicModel))
            }
        }
    }

    @Test
    fun `when getComicById called should throw an exception`() {
        runTest {
            val singleComicModel = TestUtil.createSingleComicModel(1)
            coEvery { api.getComicById(any()) } throws IOException()
            coEvery { mapper.mapToComicModel(any()) } returns singleComicModel
            coEvery { dispatcherProvider.io } returns Dispatchers.IO

            remote.getComicById(1).catch { exception ->
                assertTrue(exception is IOException)
            }.collect { _ ->

            }

            coVerify { api.getComicById(any()) }
            coVerify(exactly = 0) { mapper.mapToComicModel(any()) }
        }
    }
}