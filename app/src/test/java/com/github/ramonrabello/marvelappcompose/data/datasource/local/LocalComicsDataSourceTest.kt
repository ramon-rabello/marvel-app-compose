package com.github.ramonrabello.marvelappcompose.data.datasource.local

import com.github.ramonrabello.marvelappcompose.data.di.DispatcherProvider
import com.github.ramonrabello.marvelappcompose.util.TestUtil
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.IOException
import kotlin.jvm.Throws

class LocalComicsDataSourceTest {

    private val comicsDao = mockk<ComicsDao>(relaxed = true)
    private val dispatcherProvider = mockk<DispatcherProvider>()
    private lateinit var local:LocalComicsDataSource

    @Before
    fun initTest(){
        local = LocalComicsDataSource(comicsDao, dispatcherProvider)
    }

    @Test
    fun `when getAllComics called should retrieve the same comic list`() {
        val expectedComicList = TestUtil.createComicModelList()
        coEvery { comicsDao.getAllComics() } returns expectedComicList
        coEvery { dispatcherProvider.io } returns Dispatchers.IO

        runBlocking {
            local.getComics().collect { comics ->
                assertThat(comics, equalTo(expectedComicList))
            }

            verify { comicsDao.getAllComics() }
        }
    }

    @Test
    fun `when getAllComics called should retrieve empty comic collection`() {
        val expectedEmptyList = TestUtil.createComicModelEmptyList()
        coEvery { comicsDao.getAllComics() } returns expectedEmptyList
        coEvery { dispatcherProvider.io } returns Dispatchers.IO

        runBlocking {
            local.getComics().collect { comics ->
                assertTrue(comics.isEmpty())
            }

            verify { comicsDao.getAllComics() }
        }
    }

    @Test
    @Throws(IOException::class)
    fun `when getAllComics called should throw exception`() {
        coEvery { comicsDao.getAllComics() } throws IOException()
        coEvery { dispatcherProvider.io } returns Dispatchers.IO

        runBlocking {
            local.getComics().catch { exception ->
                assertTrue(exception is IOException)
            }.collect { _ ->

            }

            verify { comicsDao.getAllComics() }
        }
    }
}