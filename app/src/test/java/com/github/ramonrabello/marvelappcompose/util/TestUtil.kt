package com.github.ramonrabello.marvelappcompose.util

import com.github.ramonrabello.marvelappcompose.data.model.ComicCreator
import com.github.ramonrabello.marvelappcompose.data.model.ComicCreatorItem
import com.github.ramonrabello.marvelappcompose.data.model.ComicData
import com.github.ramonrabello.marvelappcompose.data.model.ComicDataContainer
import com.github.ramonrabello.marvelappcompose.data.model.ComicDataWrapper
import com.github.ramonrabello.marvelappcompose.data.model.ComicDate
import com.github.ramonrabello.marvelappcompose.data.model.ComicImage
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import com.github.ramonrabello.marvelappcompose.data.model.ComicPrice
import java.util.Calendar

object TestUtil {
    fun createComicDataWrapper(id: Long) =
        ComicDataWrapper(1, "200",
            ComicDataContainer(
                results = listOf(
                     ComicData(
                        id = id,
                        title ="Comic Title $id",
                        prices = listOf(ComicPrice("printPrice", 34.20f)),
                        dates = listOf(ComicDate("onsaleDate", Calendar.getInstance().time)),
                        creators = ComicCreator(items = listOf(ComicCreatorItem("Stan Lee", "Author"))),
                        description = "Comic description",
                        thumbnail = ComicImage("https://gateway.marvel.com/images/image2", "jpeg")
                    )
                )
            )
        )

    fun createComicModelList(): List<ComicModel> =
        listOf(
            ComicModel(1, "image1.jpeg", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f),
            ComicModel(2, "image2.jpeg", Calendar.getInstance().time, "Comic Title 2","Comic Description 2","Stan Lee", 30.50f),
            ComicModel(3, "image3.jpeg", Calendar.getInstance().time, "Comic Title 3","Comic Description 3","Stan Lee", 10.30f),
            ComicModel(4, "image4.jpeg", Calendar.getInstance().time, "Comic Title 4","Comic Description 4","Stan Lee", 80.90f),
        )

    fun createSingleComicModel(id: Long) =
        ComicModel(id, "image1.jpeg", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f)

    fun createComicModelEmptyList(): List<ComicModel> = emptyList()
}