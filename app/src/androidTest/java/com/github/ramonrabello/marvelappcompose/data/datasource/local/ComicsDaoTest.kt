package com.github.ramonrabello.marvelappcompose.data.datasource.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.github.ramonrabello.marvelappcompose.data.model.ComicModel
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.Calendar

@RunWith(AndroidJUnit4::class)
class ComicsDaoTest {
    private lateinit var comicsDao: ComicsDao
    private lateinit var db: ComicsDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ComicsDatabase::class.java).build()
        comicsDao = db.comicsDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeComicModelAndReadInList() {
        val comicModel = ComicModel(1, "image1.jpeg", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f)
        comicsDao.insert(comicModel)

        val savedComicModel = comicsDao.getComicById(1)
        assertThat(savedComicModel, equalTo(comicModel))
    }

    @Test
    @Throws(Exception::class)
    fun updateComicModelAndReadInList() {
        val comicModel = ComicModel(1, "image1", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f)
        comicsDao.insert(comicModel)

        val savedComicModel = comicsDao.getComicById(1)
        val updatedComicModel = savedComicModel.copy(title = "Comic title updated")
        assertEquals(updatedComicModel.title, "Comic title updated")
    }

    @Test
    @Throws(Exception::class)
    fun deleteComicModelAndCheckItIsNotInList() {
        val comicModel = ComicModel(1, "image1.jpeg", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f)
        comicsDao.insert(comicModel)

        val savedComicModel = comicsDao.getComicById(1)
        assertNotNull(savedComicModel)

        comicsDao.delete(comicModel)

        val deletedComicModel = comicsDao.getComicById(1)

        assertNull(deletedComicModel)
    }
    @Test
    @Throws(Exception::class)
    fun saveSomeComicModelsAndCheckIfAllTheyAreInList() {
        for(i in 1..5){
            val comicModel = ComicModel(i.toLong(), "image1.jpeg", Calendar.getInstance().time, "Comic Title 1","Comic Description 1","Stan Lee", 20.40f)
            comicsDao.insert(comicModel)
        }

        val allComics = comicsDao.getAllComics()

        assertNotNull(allComics)
        assertEquals(allComics.size, 5)
    }


}