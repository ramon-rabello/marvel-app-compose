package com.github.ramonrabello.marvelappcompose

object AndroidSDK {
    val compileSdk = 34
    val minSdk = 21
    val targetSdk = 34
}

object Versions {
    val kotlin = "1.9.2"
    val coroutines = "0.21"
    val espresso = "3.0.2"
    val junit = "4.12"
    val arch = "2.7.0"
    val coreTesting = "1.12.0"
    val room = "2.6.1"
    val testRunner = "1.5.2"
    val okhttp = "4.12.0"
    val retrofit = "2.9.0"
    val coil = "2.5.0"
    val gson = "2.8.2"
    val okio = "1.14.0"
    val mockk = "1.13.9"
    val hilt = "2.50"
    val kotlinSerialization = "1.6.0"
}

object Deps {

    // region Kotlin dependencies
    val kotlin = listOf(
        "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}",
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}",
        "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinSerialization}"
    )
    // endregion

    // region Core dependencies
    val core = listOf(
        "androidx.core:core-splashscreen:1.0.0",
        "com.google.accompanist:accompanist-placeholder-material:0.32.0"
    )
    // endregion

    // region Compose dependencies
    val compose = listOf(
        "androidx.activity:activity-compose:1.8.2",
        "androidx.compose.ui:ui",
        "androidx.compose.ui:ui-graphics",
        "androidx.compose.ui:ui-tooling-preview",
        "androidx.compose.material3:material3-android:1.2.0-rc01",
        "androidx.paging:paging-compose:3.3.0-alpha02",
        "com.airbnb.android:lottie-compose:6.0.0"
    )
    // endregion

    // region DI dependencies
    val di = listOf(
        "androidx.hilt:hilt-navigation-compose:1.0.0",
        "com.google.dagger:hilt-android:${Versions.hilt}"
    )
    // endregion

    // region Network dependencies
    val network = listOf(
        "com.google.code.gson:gson:${Versions.gson}",
        "com.squareup.okhttp3:okhttp:${Versions.okhttp}",
        "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}",
        "com.squareup.okio:okio:${Versions.okio}",
        "com.squareup.retrofit2:retrofit:${Versions.retrofit}",
        "com.squareup.retrofit2:converter-gson:${Versions.retrofit}",
        "io.coil-kt:coil-compose:${Versions.coil}"
    )
    // endregion

    // region Arch Components dependencies
    val arch = listOf(
        "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.arch}",
        "androidx.room:room-runtime:${Versions.room}",
        "androidx.room:room-ktx:${Versions.room}"
    )
    // endregion

    // region Testing dependencies
    val testing = listOf(
        "junit:junit:${Versions.junit}",
        "io.mockk:mockk:${Versions.mockk}",
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.8.0-RC",
        "com.squareup.okhttp3:mockwebserver:${Versions.okhttp}",
        "androidx.core:core-testing:${Versions.coreTesting}",
        "com.android.support.test.espresso:espresso-core:${Versions.espresso}",
        "com.android.support.test.espresso:espresso-contrib:${Versions.espresso}",
        "com.android.support.test.espresso:espresso-intents:${Versions.espresso}",
        "com.android.support.test.espresso:espresso-idling-resource:${Versions.espresso}"
    )
    // endregion
}
